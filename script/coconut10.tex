\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{10}{Finite field isomorphisms}

\begin{document}

\titlestuff
\fi

\section{Finite field isomorphisms}

We know that for any prime $p$ and for any integer $n \geq 1$, there is exactly
one finite field with $p^n$ elements \emph{up to isomorphism} and that these are
the only finite fields. In this lecture we look at some examples.

\subsection{Properties of field homomorphisms}

A field is a ring and all the information we need to make a field homomorphism
is contained in the ring structure already.

\begin{definition}[field homomorphism]
Let $\mathbb F = (F, +, \cdot)$ and $\mathbb K = (K, \oplus, \odot)$ be fields.
A function $f: F \to K$ is a field homomorphism if and only if it is a ring
homomorphism.
\end{definition}

Don't we have to check that, for example, $f(a/b) = f(a) \oslash f(b)$?
This comes for free: since $f(a/b \cdot b) = f(a)$ and know that $f(a/b) \odot
f(b) = f(a)$, we can conclude that $f(a/b) = f(a) \oslash f(b)$.

Like for groups, we can use isomorphisms to classify finite fields. But first,
here is an example of a finite field isomorphism:

\begin{proposition}
In a finite field of characteristic $p$ for a prime $p$, the Frobenius map
$\phi: x \mapsto x^p$ is an isomorphism (and therefore an automorphism).

Further, such a field has $p^n$ elements for some positive integer $n$ and
applying $\phi$ in sequence $n$ times gives the identity map, i.e. for any
field element $x$ we have $x^{(p^n)} = x$.
\end{proposition}

Another fact that we can get from the definition of field homomorphisms is that 
they preserve roots of unity: for every field homomorphism $f$ we have $f(x^k) 
= f(x)^k$ so $f(x)$ is still a $k$-th root of unity. Isomorphisms are even 
better:

\begin{proposition}
If $\mathbb F, \mathbb K$ are fields and $f: \mathbb F \to \mathbb K$ is a
field isomorphism then $f$ preserves element orders, specifically $f(x)^k = 1$
if and only if $x^k = 1$.
\end{proposition}

So if we introduce an equivalence relation on a field where two elements are
equivalent if they have the same multiplicative order, any field automorphism
can only permute elements around within the classes but never move an element
between classes.

We can also show that there can be no homomorphisms between fields of different 
characteristics. If $F$ has characteristic $c$ and $K$ has characteristic $d$ 
then
\[
    0_K = f(0_F) = f(\underbrace{1_F + \ldots + 1_F}_{c \text{ times}})
    = f(1) +_K \ldots +_K f(1)
    = 1_K + \ldots + 1_K
\]
but whether $c < d$ or $c > d$, this gives a contradiction. 
(We cannot have one characteristic divide the other, since they are both primes.)

\begin{exercise}
$(\star\star)$ \emph{Finite field inversion.}
Let $p = 1033$, this is a prime number. The exercise is to compute $1/3$ in the
finite field $\mathbb F_p$ --- without a computer (so don't just program a loop
that tries all possibilities), though you may use a calculator that supports
the modulo operation. Hint: $1031$ is $0100\;0000\;0111$ in binary.
\end{exercise}

\subsection{Finite fields as vector spaces}

Linear algebra can tell us even more about field homomorphisms. Let $F, K$ be 
finite fields of prime characteristic $p$. Both fields can be viewed as 
vector spaces over $GF(p)$, and both contain $GF(p)$ as a subfield, namely the 
\hi{multiples of 1}.

If $f: F \to K$ is a field homomorphism then because of $f(1_F) = 1_K$ the 
function $f$ must be the identity mapping on the $GF(p)$ subfields. For 
example, $f(1_F + 1_F) = f(1_F) + f(1_F) = 1_K + 1_K$ and the same for any 
other element that we can write as a sum of $1_F$ elements, which is exactly 
the subfield $GF(p)$.

A finite field $GF(p^n)$ has $p^n$ elements. We can view it as a vector space of
dimension $n$ over the \hi{base field} $GF(p)$. 
In this view, a field homomorphism is a linear function over the base field.
We have to be careful what we mean by linear here: for elements in $F$ we have
$f(a \times b) = f(a) \times f(b)$ which is not the same as the equation
$f(a \times b) = a \times f(b)$. However, if $a$ is an element of the base field
$GF(p)$ then $f(a) = a$ and so the latter equation does hold. We conclude that
a field homomorphism $f: F \to K$ between two fields over the same base field
$GF(p)$ is a $GF(p)$--linear function, but not necessarily a $F$--linear
function.

This suggests that we try and apply $f$ to a basis of $F$ as a vector space 
over $GF(p)$. If the field elements are represented as polynomials, then we can 
use the standard basis $\vec e_0 = 1, \vec e_1 = X, \vec e_2 = X^2, \ldots$.
In this notation we can write
\[ \begin{array}{rl}

    f(a_0 + a_1 X + a_2 X^2 \ldots)
    & = f(a_0) + f(a_1) f(X) + f(a_2) f(X^2) + \ldots \\
    & = a_0 + a_1 f(X) + a_2 f(X^2) + \ldots
\end{array} \]
where in the last step, we used $f(a) = a$ on the coefficients in $GF(p)$.
This already means that $f$ is completely determined by its action on the basis,
e.g. $f(1)$, $f(X)$, $f(X^2) \ldots$ but it gets even better than this: the action
of a finite field homomorphism $f$ is completely determined by the single value
$f(X)$. This is because $f(1) = 1$ and for higher powers, $f(X^n) = f(X)^n$.

In summary:

\begin{proposition}
If $F, K$ are finite fields and $f: F \to K$ is a field homomorphism then
\begin{itemize}
\item Both $F$ and $K$ have the same characteristic, call it $p$.
\item Both $F$ and $K$ can be viewed as $GF(p)$ vector spaces; their degree
as vector spaces is exactly the extension degree as fields, e.g. $n$ for a
$GF(p^n)$.
\item $f$ is the identity mapping on the subfields $GF(p)$ of $F$ and $K$
generated by $1_F$ and $1_K$ respectively.
\item The action of $f$ on any element of $F$ is completely determined by
the value of $f(X)$, when $F$ is written out as polynomials.
\end{itemize}
\end{proposition}

\subsection{Automorphisms of $GF(7^2)$}

Let's find the automorphisms of $GF(7)[X]/(X^2+X+6)$, that is the invertible 
functions $f$ on this domain with $f(x+y) = f(x) + f(y)$, $f(0) = 0$, $f(1) = 
1$ and $f(xy) = f(x)f(y)$. All that we need to determine an automorphism is 
$f(X)$, since for any element $(a, b)$ of the field we have $f(a+bX) = a + b 
\times f(X)$.

We start with setting $f(X) = u + vX$ for variables $u, v$. Then we have $f(X)
\times f(X) = (u+vX)(u+vX) = u^2 + 2uvX + v^2(X^2+X+6) - v^2(X+6) = (u^2+v^2) +
(2uv-v^2)X$. However, we also have $f(X)\times f(X) = f(X^2) = f(-X-6) = (1-u) -
vX$, giving us the equations 
\[ \left| \begin{array}{rcl}
1-u & = & u^2+v^2 \\
-v & = & 2uv - v^2
\end{array} \right| \]

The last equation gives us two cases: either $v = 0$, which is definitely not 
an automorphism, or $v \neq 0$ in which case we divide by $v$ to get $-1 = 
2u-v$ and substitute to get the quadratic equation $1-u = u^2 + (2u+1)^2$ which 
gives us the solutions $u = 0, v = 1$ and $u = 6, v = 6$. Our automorphisms are 
$f_1(X) = X$ and $f_2(X) = 6 + 6X$, from which we find $f_1(a + bX) = a + bX$ 
--- the identity function, which is not surprising --- and $f_2(a+bX) = (a+6b) 
+ 6bX$.

We can of course write these linear functions as matrices. For $a + bX = (a, b)^T$
we get
\[
    f_1 = \m{1 & 0 \\ 0 & 1}
    \hspace{2em}
    f_2 = \m{1 & 6 \\ 0 & 6}
\]

\begin{exercise}
$(\star)$ Why can $v = 0$ in the above calculation not yield an automorphism?
\end{exercise}

\subsection{The automorphism group}

The group of automorphisms of a finite field can be found quicker than in the
above calculation with the following theorem:

\begin{theorem}
The group of automorphisms of $GF(p^n)$ is isomorphic to the group $(\mathbb
Z_n, +)$ and the Frobenius map $f(X) = X^p$ is one of its generators.
\end{theorem}

In a finite field represented as $GF(p)[X]/q(X)$, we can compute $f(X)$ for all
the automorphisms by repeatedly applying the Frobenius map giving $X^p, X^{p^2},
X^{p^3}, \ldots$ and reducing modulo $q(X)$.

\textbf{Example.}
In the example in the last section, the Frobenius map is $f(X) = X^7$ which
reduces modulo $X^2 + X + 6$ and modulo 7 as $6 + 6X$. This is exactly the map
that we found by hand.

\textbf{Example.} Compute $f(X)$ for the automorphisms of $GF(2^8)$ in the
representations modulo the polynomials $p(X) = X^8 + X^4 + X^3 + X + 1$ and
$q(Y) = Y^8 + Y^4 + Y^3 + Y^2 + 1$.

In our case, $p = 2$ so we have to compute the sequence $X, X^2, X^4, \ldots$
reducing whenever the degree goes to 8 or more.

\begin{center}
\begin{tabular}{lll}
$n$ & mod $p(X)$ & mod $q(Y)$ \\
\midrule
0 & $X$ & $Y$ \\
1 & $X^2$ & $Y^2$ \\
2 & $X^4$ & $Y^4$ \\
3 & $X^4+X^3+X+1$ & $Y^4+Y^3+Y^2+1$ \\
4 & $X^6+X^4+X^3+X^2+X$ & $Y^6+Y^3+Y^2$ \\
5 & $X^7+X^6+X^5+X^2$ & $Y^7+Y^4+Y^3+Y^2+1$ \\
6 & $X^6+X^3+X^2+1$ & $Y^6+Y^4+Y^3+Y^2+Y+1$ \\
7 & $X^7+X^6+X^5+X^4+X^3+X$ & $Y^7+Y^2+1$
\end{tabular}
\end{center}

\subsection{Automorphisms of $GF(2^3)$}

For another example, consider $GF(2^3)$, this time with the irreducible 
polynomial $p(X) = X^3 + X + 1$. The Frobenius map sends $X \mapsto X^2$ and 
$X^2 \mapsto [X^4] = X^2 + X$. As a matrix, we get
\[
\phi = \m{1 & 0 & 0 \\ 0 & 0 & 1 \\ 0 & 1 & 1}, \qquad
\phi \m{a \\ b \\ c} = \m{a \\ c \\ b + c}
\]
from which we read off $\phi(a + bX + cX^2) = a + cX + (b+c)X^2$. Finding the
other automorphisms is easy too:
\[
\phi^2 = \m{1 & 0 & 0 \\ 0 & 0 & 1 \\ 0 & 1 & 1} \cdot
\m{1 & 0 & 0 \\ 0 & 0 & 1 \\ 0 & 1 & 1} =
\m{1 & 0 & 0 \\ 0 & 1 & 1 \\ 0 & 1 & 0}
\]
which maps $X \mapsto X + X^2$ (middle column) and $X^2 \mapsto X$ (right
column). Multiplying with the column vector $(a; b; c)$ we get $\phi^2(a + bX +
cX^2) = a + (b+c)X + b X^2$. If we look at the third power
\[
\phi \cdot \phi^2 = \m{1 & 0 & 0 \\ 0 & 0 & 1 \\ 0 & 1 & 1} \cdot
\m{1 & 0 & 0 \\ 0 & 1 & 1 \\ 0 & 1 & 0} =
\m{1 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1}
\]
we get the identity map back, as expected.




%\subsection{Isomorphisms of $GF(2^8)$}

%Next, let's look for the isomorphisms of $GF(2^8)$ from the representation
%modulo $p(X) = X^8 + X^4 + X^3 + X + 1$ to another representation, for example
%$q(Y) = Y^8 + Y^4 + Y^3 + Y^2 + 1$ (this is another irreducible polynomial;
%note the $Y^2$ in place of the $X$).
%That is, we're looking for an isomorphism $f$ that maps field elements to other
%field elements such that $f(a \times b) = f(a) \odot f(b)$ where $\odot$ is
%multiplication modulo $q(Y)$ and $\times$ is multiplication modulo $p(X)$.
%Again, the value $f(X)$ will determine an isomorphism $f$ from the
%representation modulo $p(X)$ to the representation modulo $q(Y)$.

%If we find any one isomorphism from $GF(2)[X]/p(X)$ to $GF(2)[X]/q(Y)$ then we
%can get the whole set of isomorphisms by composing our one isomorphism with
%these automorphisms.

%To get an isomorphism $f$, it is enough to find $f(X)$ which completely
%determines the isomorphism. To find this, we have to briefly work with $p$ as a
%polynomial over $GF(2^8)$. So far, we have considered polynomials over $GF(2)$
%to represent elements of $GF(2^8)$, i.e. our polynomials had coefficients in
%$GF(2)$. Now, we consider polynomials with coefficients in $GF(2^8)$. This can
%seem confusing at first because we will need two variable symbols: one to
%represent the ``variable'' of the polynomial and one to represent elements of
%$GF(2^8)$. For example, if $a$ and $b$ are elements of $GF(2^8)$ then $f(X) =
%aX+b$ is a polynomial over $GF(2^8)$ with coeffcients $a, b$. Suppose that $a =
%Y^2 + 1$ and $b = 2Y$, so we are using the letter $Y$ to represent elements,
%then $f(X) = (Y^2+1)X + 2Y$.  $f$ is still a degree-one polynomial in one
%variable $X$; we just needed another variable symbol $Y$ to write some of the
%coefficients which are elements of $GF(2^8)$.

%In $GF(2^8)$ represented modulo $p$, we have $p(X) = 0$. So for any isomorphism
%$f$ out of this representation we also have $f(p(X)) = 0$. But since $f$ is an
%isomorphism we must also have $p(f(X)) = 0$. This means that $f(X)$, which is
%an element of the representation modulo $q$, must be a zero of the polynomial
%$p$ modulo $q$. So our recipe for finding isomorphisms is to factor the
%polynomial $p(X)$ in the representation modulo $q(Y)$, where we treat $X$ as a
%variable. In other words, we are looking for elements $b$ such that $(X - b)$
%divides $p(X)$ modulo $q(Y)$.

%\begin{theorem}
%A function $f$ is an isomorphism from $GF(z^n)$ represented modulo $p(X)$ to
%$GF(z^n)$ represented modulo $q(Y)$ if and only if $f$ commutes with addition
%and multiplication, $f(1) = 1$ and for $b = f(X)$, $(X - b)$ divides $p(X)$ as
%polynomials modulo $q(Y)$.
%\end{theorem}

%One of the isomorphisms has $b = Y + 1$.
%We can check this by computing $p(X) / (X - (Y+1))$ modulo $q(Y)$ and find

%\[
%\begin{array}{lrl}
%\multicolumn{3}{l}{(X^8 + X^4 + X^3 + X + 1) = (X - (Y+1))\times(} \\
%& 1 & X^7 \\
%& + (Y+1) & X^6 \\
%& + (Y^2+1) & X^5 \\
%& + (Y^3+Y^2+Y+1)&  X^4 \\
%& + Y^4 & X^3 \\
%& + (Y^5 + Y^4+1) & X^2 \\
%& + (Y^6 + Y^4 + Y + 1) & X \\
%& + (Y^7 + Y^6 + Y^5 + Y^4 + Y^2) \\
%) \pmod{q(Y)}
%\end{array}
%\]

%\newcommand{\x}[1]{\texttt{0x#1\,}}

%If the mixture of $X$ and $Y$ variables is confusing, we can also represent
%elements of $GF(2)[Y]/(Y+1)$ as two-digit hexadecimal numbers. In this case, $b
%= \x{03}$ and the above equation is
%\[
%\begin{array}{l}
%(X^8 + X^4 + X^3 + X + 1) = (X - \x{03})(X^7 + \x{03} X^6 + \x{05} X^5 +
%\x{0f} X^4 + \\ \x{10} X^3 + \x{31} X^2 + \x{53} X + \x{f4}) \pmod{q(Y)}
%\end{array}
%\]


%\subsection{Isomorphisms in $GF(7^2)$}

%We said that there is only one finite field $GF(p^n)$ up to isomorphism for
%each prime $p$ and positive integer $n$. Let's look at some examples of this
%too.

%For $GF(7^2)$, another representation of the same field comes from choosing a
%different irreducible polynomial, such as $Y^2 + 1$ which gives the
%multiplication $(a, b) \odot (c, d) = (ac-bd, ad+bc)$.

%Let's try and compute the isomorphisms
%\[f: GF(7)[X]/(X^2+X+6) \to GF(7)[Y]/(Y^2+1) \]
%We will use the symbol $X$ for elements in the first representation and
%the symbol $Y$ for elements in the second; this way the symbol name tells us
%which polynomial we have to use when reducing elements after multiplication.

%We know that $f(1, 0) = (1, 0)$ and thus that $f(a, 0) =
%(a, 0)$ for any field element $a \in GF(7)$ since $1$ is a generator of
%$(\mathbb Z_7, +)$. So all we need to find is $f(0, 1) = f(X)$ since $f(a + bX)
%= a + b \times f(X)$. Writing $f(X) = u + vX$ for variables $u, v$ ranging over
%$GF(7)$, for any element $(a, b)$ we have $f(a, b) = (a + ub, vb)$.
%Now look at the equation $f(a, b) \odot f(c, d) = f( (a, b) \times (c, d))$
%that any isomorphism must satisfy. Writing this out and combining terms gives
%the conditions $2uv = -v$ and $u^2 - v^2 = 1 - u$ which give $u = 3$ and $v = 2
%\vee v = 5$.  So we have two isomorphisms
%\[\begin{array}{l}
%f_1: (a, b) \mapsto (a + 3b, 2b) \\
%f_2: (a, b) \mapsto (a+3b, 5b)
%\end{array}\]
%We can describe both these isomorphisms by their action on the polynomial $(0,
%1)$ that represents the monomial $X$: $Y_1 = f_1(X) = 3+2X$ and $Y_2 = f_2(X) =
%3+5X$.

\begin{exercise}
$(\star\star)$ \emph{The field $GF(5^2)$.}
\begin{enumerate}
\item Find the explicit multiplication formula for the representation of
$GF(5^2)$ modulo the irreducible polynomial $X^2 + 2X + 3$.
\item Do the same for the irreducible polynomial $2Y^2 + 4Y + 1$.
%\item Find the isomorphisms from the first representation to the second.
\end{enumerate}
\end{exercise}

\ifdefined\booklet
\else
\end{document}
\fi
