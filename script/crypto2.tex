\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{22}{Secret sharing}

\begin{document}

\titlestuff
\fi

\section{Secret sharing}

You own a bicycle with a lock that has a 4-digit code. You would like to let
two of your colleagues together unlock your bicycle in an emergency, but you do
not want either of them to be able to unlock it on their own. How can you
achieve this? (The example with a bicycle lock may be contrived, but providing
emergency access to your smartphone or social media accounts in case you suffer
an accident may be a more realistic concern. Or imagine you want to back up your
secret keys in two different locations, e.g. home and workplace, such that a
thief would have to break into both to get your secret key.)

A silly solution is to give one colleague the first two digits of your code and
another one the last two digits. In this case, instead of $10^4$ possibilites,
each colleague on their own has to check only $10^2$ remaining possibilities.
In terms of entropies, for a thief with no information, your lock has
$\log_2(10^4) \approx 13.28$ bits of entropy but for each of your colleagues,
only $\log_2(10^2) \approx 6.64$ bits of entropy remain.

\subsection{A first scheme}

A better solution is to work in $\mathbb Z_{10^4}$ (or indeed any finite group).
Let $c$ be your code. Pick a random 4-digit number $a$ and give this to the
first colleague. Give the second colleague $b = c - a \bmod 10^4$ so both of
them together can reconstruct $c = a + b \bmod 10^4$ if required.

It is clear that $a$ on its own contains no information on $c$, since it was
drawn independently. It is also clear that $a$ and $b$ together are enough to
recover $c$. For $b$ on its own, suppose that your colleague receives $b = 2215$.
Then they can reason like this:
\begin{enumerate}
\item If $a = 0000$, then $c = 2215$.
\item If $a = 0001$, then $c = 2216$.
\item If $a = 0002$, then $c = 2217$.
\item \ldots 9997 more cases \ldots
\end{enumerate}

Since $a$ was uniform, each of these 10000 cases is equally likely and so your
colleage who knows $b$ has to try out 10000 possibilities to open your lock ---
which is exactly the same as a thief who has no information on your code at all.
The above argument assumes that $a$ is uniform, but it does not depend on the
distribution of $c$. If locks from a particular brand have a code that never has
a 1 as the first digit for example, then a thief and your second colleague are
still in exactly the same position.

In terms of probability theory, writing $ABC$ for the random variables describing
the distribution of $abc$ we have for all values of $a, b, c$ that

\[
    P(C = c \mid B = b) = P(C = c \mid A = a) = P(C = c)
\]

To show $P(C = c \mid A = a) = P(C = c)$ we can simply invoke independence of
$A$ and $C$ to get $P(C = c \mid A = a) = P(C = c) \times P(A = a)/P(A = a) = P(C = c)$.

For $B$, we apply the cryptographer's theorem: $A$ is uniform and independent
of $C$. Therefore $Z = (-A)$ is also uniform and independent of $C$. Using the
theorem gives that $C + Z$ is uniform and indepdendent of $C$, and $C + Z = B$. 

Secret sharing generalises to more than two colleages: for $n$ people, pick
$n-1$ random numbers $a_1, \ldots, a_{n-1}$ independently and uniformly and set
$a_n = c - (a_1 + \ldots + a_{n-1})$. The result is that all $n$ values $a_i$
let you compute the secret $c$ but it is independent of any subset of up to
$n-1$ of these values.

\subsection{Shamir secret sharing}

What if you want to distribute a secret $c$ among $n$ people such that any $k$
of them should be able to reconstruct it, but any group of at most $k-1$ people
should get no information on the secret? Here $n$ can be an arbitrary integer
greater than $0$ and $k$ can be any integer less or equal to $n$.

Adi Shamir presented a solution to this problem in 1979 in a paper called
\emph{How to Share a Secret} that was one of the foundational papers of modern
cryptography. The scheme he presented is called \hi{Shamir secret sharing} or
\hi{polynomial secret sharing}.

Shamir's secret sharing scheme requires the secret to be an element of a finite
field with at least $n$ elements. This is not a problem in practice since any
$k$-bit string can be viewed as an element of $GF(2^k)$ and if $n > 2^k$ we can
view a $k$-bit string as an element of a larger field by adding trailing zeroes
or using some other encoding.

\begin{definition}
Let $F$ be a finite field with at least $n+1$ elements containing the secret $c$.
Shamir's secret sharing is as follows.
\begin{itemize}
\item Pick a random polynomial $p$ of degree-bound $k-1$ subject to $p(0) = c$.
That is, set $a_0 = c$ and pick constants
$a_1, \ldots, a_{k-1}$ independently and uniformly at random from $F$ and
define the polynomial to be
\[
    p(X) = a_0 + a_1 \times X + a_2 \times X^2 + \ldots + a_{k-1} \times X^{k-1}
\]
\item Pick $n$ distinct and nonzero points $x_1, \ldots, x_n$. The points do not
need to be picked uniformly at random.
\item Give each person a different point $(x_i, p(x_i))$ for $i = 1, \ldots, n$.
\end{itemize}
Any set of at least $k$ people can interpolate the polynomial $p$ from their
$k$ points, since the degree-bound is $k-1$. Then they can compute $c = p(0)$.
\end{definition}

\subsection{Facts about random polynomials}

To prove that this scheme gives no information to any set of at most $k-1$
people, we need some linear algebra. What we are really doing here is converting
polynomials of degree-bound $k-1$ back and forth between two bases, the standard
basis with the coefficients $a_i$ and the basis in which a polynomial $p$ is
defined by its values at $k$ points e.g. 
$p(0), p(x_1), p(x_2), \ldots, p(x_{k-1})$.

\begin{proposition}
Let $d \geq 0$ be an integer, let $F$ be a finite field with at least $d+1$
elements and let $\Pi^d(F)$ be the set of all polynomials of degree $\leq d$
over $F$. Then
\begin{enumerate}
\item For every set of $d+1$ points $(x_i, y_i)_{i=0}^{d}$ with distinct $x_i$
and arbitrary $y_i$, there is exactly one polynomial in $\Pi^d(F)$ going through
all these points.
\item For every pair of values $x \in F, y \in F$ the probability that a randomly
chosen polynomial $p$ in $\Pi^d(F)$ has $p(x) = y$ is $1/|F|$. A randomly chosen
polynomial here means that the coefficients $a_0, \ldots, a_d$ are picked
uniformly at random and independently from each other.
\item For a randomly chosen polynomial $p \in \Pi^d(F)$, and any set of
$d+1$ points $(x_i, y_i)_{i=0}^{d}$ with distinct $x_i$ and arbitrary $y_i$,
conditioned on the event that $p(x_i) = y_i$ for $i > 0$ the probability that
$p(x_0) = y_0$ is still $1/|F|$.
\end{enumerate}
\end{proposition}

For the first point, we begin by observing that for any set of points there is
at least one polynomial of degree-bound $d$ going through these points, namely
the one found by Lagrange interpolation:
\[
    p(X) = \sum_{i=0}^{d} y_i \times \prod_{j \neq i} \frac{X - x_j}{x_i - x_j}
\]
This defines a polynomial of degree-bound $d$ since none of the products has a
higher degree; at any of the points $x_k$ the summands for all but the term with
$i = k$ have an $(x_k - x_k) = 0$ term in the numerator and in the remaining
one, the numerator and denominator are equal
(each factor is $(x_i - x_j)/(x_i-x_j)$) so the term under the product has the
value one, leaving out the factor $y_k$ at the front. Therefore $p(x_k) = y_k$.

For any set of $d+1$ distinct points $x_0, x_1, \ldots, x_d$ there are 
$|F|^{d+1}$ possible values for the values $y_0, y_1, \ldots, y_d$. This means 
that there must be $|F|^{d+1}$ distinct polynomials in $\Pi^d(F)$ that we can 
find through Lagrange interpolation on a fixed set of $x$-values, but from the
representation with the usual coefficients we also know that
$|\Pi^d(F)| = |F|^{d+1}$ so there cannot be two different polynomials of
degree-bound $d$ that go through the same $d+1$ points $(x_i, y_i)$, by the
pigeonhole principle.

For the second claim, what we really need to show is that of the $|F|^{d+1}$
polynomials in $\Pi^d(F)$, exactly $|F|^d$ go through the point $(x, y)$.
Set $x_0 = x$ and pick any $d$ distinct points $x_1, \ldots, x_d$ that are also 
distinct from $x_0$. This is possible as we demanded that the field $F$ have at 
least $d + 1$ elements.

If we pick a polynomial in $\Pi^d(F)$ by choosing uniformly random values for 
the points $p(x_0), p(x_1), \ldots, p(x_d)$ then we still get a uniformly 
random polynomial with the same distribution as before as the function that 
maps between these two bases is bijective. But in this new basis, it is clear 
that since $p(x_0)$ is itself picked uniformly at random and independent of the 
values at the other points, then for any $y \in F$ the probability that $p(x_0) 
= y$ is $1/|F|$.

This argument also proves the third claim as we are conditioning on an event
that is independent of choosing the value at $p(x_0)$.

\subsection{Proof for Shamir's secret sharing}

To prove that Shamir's secret sharing reveals no information on the secret $c$
to any group of at most $k-1$ colleagues, let $C$ be a random variable for the
secret $c = p(0)$. Fix any $k-1$ values $x_1, \ldots, x_{k-1}$
and let $Y_1, \ldots, Y_{k-1}$ be random variables for the values of the
polynomial at these points. Let $Y$ be a random variable for the
joint distribution of $Y_1, \ldots, Y_{k-1}$. We need to show
\[
    P(C = c \mid Y = y) = P(C = c)
\]
for all $c \in F, y \in F^{k-1}$. Note that we are not assuming that $C$ is
uniformly distributed --- the scheme must be secure for any distribution on the
secret value.

Consider the basis transformation that turns a polynomial represented by
coefficients $(a_0, a_1, \ldots, a_{k-1})$ into one represented by
$(y_0, y_1, \ldots, y_{k-1})$ where $y_0 = p(0)$ and $y_i = p(x_i)$ for $i > 0$.
We picked our polynomial from the subspace of all polynomials where $p(0) = c$,
which is equivalent to $a_0 = c$ and $y_0 = c$. Within this subspace, we can
write a polynomial with only the coordinates $(a_1, \ldots, a_{k-1})$ resp.
$(y_1, \ldots, y_{k-1})$ and since the basis transformation is bijective, this
means that each vector $(y_1, \ldots, y_{k-1})$ occurs with equal probability.
Therefore, $P(Y = y) = 1/|F|^{k-1}$ for all $y \in F^{k-1}$. Further, since the
$a_i$ resp. $y_i$ for $i \geq 1$ are independent of $a_0$ resp. $y_0$, we even
have $P(Y = y \mid C = c) = 1/|F|^{k-1}$.

Bayes' theorem gives us
\[
    P(C = c \mid Y = y) \times P(Y = y) = P(Y = y \mid C = c) \times P(C = c)
\]
From this we can conclude that $P(C = c \mid Y = y) = P(C = c)$ as the other
two terms are both $1/|F|^{k-1}$. This completes the proof.

There is a variation of Shamir's secret sharing scheme that is easier to prove:
pick a completely random polynomial $p \in \Pi^{k-1}(F)$, give each person a
distinct point $x_i \neq 0$ as before and then give everyone the value
$s = c + p(0)$. For any set of up to $k-1$ people, the value of $p(0)$ is
uniformly random and independent of $c$, therefore by the cryptographer's
theorem the value of $s$ is also random and independent of $c$. To be precise,
the joint distribution of $s$ and all the $k-1$ known points is uniformly random
and independent of $c$.

In entropy terms, what we have shown is that $H(C \mid Y) = H(C)$ where $Y$ is
the distribution of polynomial values at up to $k-1$ points (whose $x$-value is
nonzero).

\ifdefined\booklet
\else
\end{document}
\fi
