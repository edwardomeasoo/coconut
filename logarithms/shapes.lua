-- draw SVG shapes for logarithms figures

xml = require 'xmlgen'

-- call a function and write to a file
function write(filename, d)
    local file
    file = assert(io.open(filename, "w"))
    assert(file:write(d))
    file:close()
end

-- SVG drawing
function svg(f)
    local x = {}
    table.insert(x, '<?xml version="1.0" encoding="UTF-8" ?>')
    table.insert(x, '<svg xmlns="http://www.w3.org/2000/svg" version="1.1">')
    f(x)
    table.insert(x, '</svg>')
    return table.concat(x, "\n")
end

function cm(s) return s .. "cm" end

-- text, with default style
function text(x, y, t, params)
    local data = {
        ['font-family'] = 'Open Sans',
        fill = 'black',
        ['font-size'] = '18pt',
        ['text-anchor'] = middle,
        ['alignment-baseline'] = 'middle',
        x = cm(x),
        y = cm(y),
        ['_'] = t
    }
    if params then
        for k,v in pairs(params) do data[k] = v end
    end
    return xml.node('text')(data)
end

local SCALE = 35.43307 -- user coords to cm

-- rectangle
function rect(x0, y0, x1, y1, p)
    local d = table.concat({
        'M', (x0*SCALE), (y0*SCALE),
        'L', (x0*SCALE), (y1*SCALE),
        'L', (x1*SCALE), (y1*SCALE),
        'L', (x1*SCALE), (y0*SCALE),
        'Z'
    }, ' ')
    local data = {
        stroke = 'black',
        fill = 'white',
        ['stroke-width'] = '1pt',
        d = d
    }
    if p then
        for k,v in pairs(p) do data[k] = v end
    end
    return xml.node('path')(data)
end

-- simple line
function line(x0, y0, x1, y1, p)
    local d = 'M' .. (x0*SCALE) .. ' ' .. (y0*SCALE) .. ' ' ..
              'L' .. (x1*SCALE) .. ' ' .. (y1*SCALE)
    local data = {
        stroke = 'black',
        ['stroke-width'] = '1pt',
        d = d
    }
    if p then
        for k,v in pairs(p) do data[k] = v end
    end
    return xml.node('path')(data)
end

-- rods example
function rods()
    write('rods.svg', svg(function(t)
        table.insert(t, rect(0, 0, 3, 1, {fill = 'limegreen'}))
        table.insert(t, rect(3, 0, 8, 1, {fill = 'yellow'}))
        table.insert(t, text(1.25, 0.75, "3"))
        table.insert(t, text(5, 0.75, "5"))       
        table.insert(t, rect(0, 1.5, 8, 2.5, {fill = 'maroon'}))
        table.insert(t, text(3.5, 2.25, "8", {fill = 'white'}))
    end))
end

-- rulers example
function rulers()
    write('rulers.svg', svg(function(t)
        table.insert(t, rect(0, 0, 16, 1))
        for i = 0, 15 do
            table.insert(t, line(0.5 + i, 0.8, 0.5 + i, 1))
            table.insert(t, line(0.5 + i, 0.2, 0.5 + i, 0))
            table.insert(t, text(0.4 + i, 0.6, i, {['font-size'] = '10pt'}))
        end
        
        table.insert(t, rect(3, 1.25, 19, 2.25))
        for i = 0, 15 do
            table.insert(t, line(3.5 + i, 1.45, 3.5 + i, 1.25))
            table.insert(t, line(3.5 + i, 2.05, 3.5 + i, 2.25))
            table.insert(t, text(3.4 + i, 1.9, i, {['font-size'] = '10pt'}))
        end
        
        local arrow = {stroke = 'red', ['stroke-width'] = '2pt'}
        
        table.insert(t, line(3.5, 0.75, 3.5, 1.5, arrow))
        table.insert(t, line(3.35, 1.25, 3.5, 1.5, arrow))
        table.insert(t, line(3.65, 1.25, 3.5, 1.5, arrow))
        
        table.insert(t, line(8.5, 0.75, 8.5, 1.5, arrow))
        table.insert(t, line(8.35, 1, 8.5, 0.75, arrow))
        table.insert(t, line(8.65, 1, 8.5, 0.755, arrow))
    end))
end

-- powers of 2 multiplication rods
function mulrods1()
    write('mulrods1.svg', svg(function(t)
        table.insert(t, rect(0, 0, 1, 1))
        table.insert(t, text(0.25, 0.75, "2"))
        table.insert(t, rect(1, 0, 2, 1))
        table.insert(t, text(1.25, 0.75, "2"))
        table.insert(t, rect(2, 0, 3, 1))
        table.insert(t, text(2.25, 0.75, "2"))
        table.insert(t, rect(3, 0, 4, 1))
        table.insert(t, text(3.25, 0.75, "2"))
        
        table.insert(t, rect(0, 1.25, 2, 2.25))
        table.insert(t, text(0.25, 2, "4"))
        table.insert(t, rect(2, 1.25, 4, 2.25))
        table.insert(t, text(2.25, 2, "4"))
        
        table.insert(t, rect(0, 2.5, 3, 3.5))
        table.insert(t, text(0.25, 3.25, "8"))
        
        table.insert(t, rect(0, 3.75, 4, 4.75))
        table.insert(t, text(0.25, 4.5, "16"))
    end))
end

-- slide rule, 3 x 4 = 12
function sliderule()
    write('sliderule.svg', svg(function(t)
        local S = 3.8
        local function ruler()
            table.insert(t, rect(0, 0, 16, 1))
            for i = 1, 15 do
                local y = S * math.log(i)/math.log(2)
                table.insert(t, line(0.5 + y, 0.8, 0.5 + y, 1))
                table.insert(t, line(0.5 + y, 0.2, 0.5 + y, 0))
                table.insert(t, text(0.4 + y, 0.6, i, {['font-size'] = '10pt'}))
            end
        end
        ruler()
        table.insert(t, '<g transform="translate(' ..
            SCALE * (S * math.log(3)/math.log(2)) .. ',' ..
            SCALE * 1.25 .. ')">')
            ruler()
        table.insert(t, '</g>')
        
        local arrow = {stroke = 'red', ['stroke-width'] = '2pt'}
        
        local y = S * math.log(3)/math.log(2)
        table.insert(t, line(y + 0.5, 0.75, y + 0.5, 1.5, arrow))
        table.insert(t, line(y + 0.65, 1.25, y + 0.5, 1.5, arrow))
        table.insert(t, line(y + 0.35, 1.25, y + 0.5, 1.5, arrow))
        
        y = S * math.log(12)/math.log(2)
        table.insert(t, line(y + 0.5, 0.75, y + 0.5, 1.5, arrow))
        table.insert(t, line(y + 0.65, 1, y + 0.5, 0.75, arrow))
        table.insert(t, line(y + 0.35, 1, y + 0.5, 0.755, arrow))
    end))
end

