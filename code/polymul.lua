
function newpoly(list)
    local r = {}
    local d = 0
    for i = 1, #list do
        r[i-1] = (list[i] % 2)
        if list[i] ~= 0 then d = i - 1 end
    end
    r.deg = d
    return r
end

function plus(a, b)
    local r = {}
    local d = 0
    for i = 0, math.max(a.deg, b.deg) do
        r[i] = ((a[i] or 0) + (b[i] or 0)) % 2
        if r[i] ~= 0 then d = i end
    end
    r.deg = d
    return r
end

function times(a, b)
    local r = {deg = 0, [0] = 0}
    local d = 0
    for i = 0, b.deg do
        if b[i] ~= 0 then
            for j = 0, a.deg do
                r[j + i] = ((r[j + i] or 0) + a[j]) % 2
                if r[j+i] ~= 0 then
                    d = math.max(d,j+i)
                end
            end
        end
    end
    r.deg = d
    for i= 1, d do
        r[i] = (r[i] or 0)
    end
    return r
end

function p(x)
    local s = "("
    for i = 0, x.deg do
        s = s .. x[i] .. " "
    end
    s = s .. ")"
    print(s)
end

