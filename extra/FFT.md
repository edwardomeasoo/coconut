The fast Fourier transform
==========================

# 1. Why?

A signal is, very broadly speaking, something that transmits information over time. Often the information you're interested in comes from a continuous process, but you make it discrete (and so easier to work with) by *sampling* at regular intervals, for example:

  * A temperature sensor that reports how cold your fridge is, once a minute.
  * A GPS device on a bus that sends its position in regular intervals to a computer, to update the electronic timetables.
  * To store music on a computer in MP3 format, you record the sound wave and sample it - normally around 44100 times a second.

The result of such a sampling process is a sequence of points `(x_i, y_i)` where `x_i` is the time at which the sample was taken and `y_i` is the sample value (temperature, position, volume etc.). Of course if you know the sampling rate, you don't have to store all the x-values.

How do we get information out of a signal? The simplest thing you can do is probe it, that is look up the value at a particular point. This tells you for example that your fridge was 5 degrees celsius at 10am. But for a lot of signals, the information you want is not that readily available.

We had a great physics lecture one day where the professor got someone in to play the trumpet, and in the background there were two screens that showed live information. The first one just recorded the sound and showed the sound wave - it got bigger if the trumpet played more loudly, but apart from that you couldn't read anything interesting off the graph directly.

The second screen showed a transformed version of the live graph which showed the notes being played - if it was a C note then there was a spike at one point, for a D note a spike a bit further to the right.

# 2. The Fourier transform

The calculation that turned the sound wave into the graph where you could read off the notes is the Fourier transform.
A musical note in its most basic form (as you'd get off a tuning fork) is a sound wave at a constant frequency - the middle A note on a piano (technically, `A4`) has a frequency of 440 Hertz. Doubling the frequency raises the note one octave, halving the frequency lowers it one octave.

(At least, middle A is 440 Hz today. There's some debate in classical music about whether music should be played with instruments as we tune them today, or as we did in the time of the original composers. There's some evidence that Beethoven's piano had an A tuned to around 421 Hz, closer to today's A-flat. But I digress.)

If you play a chord of several notes at once, then the sound wave you get is in a certain sense the sum of the waves of the individual notes. Additionally, a note made by a real instrument will carry a lot of extra wave components beside the "main" one, which is why an A on a piano sounds different from an A on a trumpet.

How do you extract an individual note from a sound wave? If you could write a test to see if someone's currently playing a particular note or not, then you could just run the tests for all different notes in parallel on the sound wave to get the notes back. So how do we detect whether a particular frequency appears - and if so, how loudly - in a sound wave?

# 3. Convolution

The operation that probes for a particular pattern in a sound wave is called convoluion. You take the wave you're analysing, and a separate sound wave that is the pattern you're looking for, and in algebraic terms you multiply the two waves together. If the pattern doesn't appear in the original wave, multiplication cancels everything out and you get 0; if the pattern appears, you get a nonzero value.

To teach the theory of what's going on, one often forgets about the sampling and thinks about multiplying the two original, continuous sound waves. In this case the multiplication is a kind of integral of the product of the two wave functions, called a convolution. The idea is that if the waves are at different frequencies, the resulting function you take the integral of will have as much "mass" above the 0-line as below, causing it to cancel out, whereas if the functions have a common frequency then it'll cause much more of the result to be positive than negative.

In the discrete world, if we have a sequence of N values (e.g. the signal value at N different points in time), we can think of these N values as defining a vector of length N. To extract a particular component from a vector, you can take the inner product with a basis vector that has a 1 at the component you're after, for example to get the third component of a vector

```
(0, 0, 1, 0) * (a_1, a_2, a_3, a_4) = 0 a_1 + 0 a_2 + 1 a_3 + 0 a_4 = a_3
```

This works fine if the information you're looking for is in exactly one component of your vector, but it's more interesting than that.

Imagine that your N-component vector represents the coefficients of a degree N-1 polynomial, e.g. `(a_0, a_1, a_2 ...) = a_0 + a_1 X + a_2 X^2 ...`.

If the information you're looking for is the value of the polynomial at a point with x-value `w`, then you can compute this too as an inner product:

```
(1, w, w^2, w^3) * (a_0, a_1, a_2, a_3) = a_0 + a_1 w + a_2 w^2 + a_3 w^3
```

which is exactly the evaluation of the polynomial for `X = w`. It is also exactly the row of the Vandermonde matrix (in row format) for the point `w`.
This is not a coincidence because nothing is ever a coincidence in mathematics.

And this is exactly the discrete version of convolution (or rather, convolution is the continuous analogue of this). Convolution is a linear operation on functions, and if you use it as multiplication then you get something like a ring: it is associative and distributes over addition. You have to cheat a bit for the neutral element though. On certain sets of functions you even get an inverse.

The discrete version of convolution is *exactly* the multiplication of polynomials. Specifically, two polynomials in coefficient form multiply as

```
(a_0, a_1, a_2 ...) x (b_0, b_1, b_2 ...) = (c_0, c_1, c_2 ...)
with
c_i = SUM_j a_j x b_{i-j}
```

and the convolution formula is, with S representing the integral sign,

```
(f*g)(x) = S f(u)g(x-u) du
```

# 4. The discrete Fourier transform

The discrete Fourier transform is nothing other than turning a set of N values that you think of as polynomial coefficients into the evaluation of the polynomial at particular N points. In other words, multiplying with a Vandermonde matrix.
However, this is usually done over the complex numbers rather than a finite field, and the points are very particular ones, namely the N-th roots of unity. But it's still just a Vandermonde matrix.

So we can write the DFT with the following code:
```
input: x_0, ..., x_{N-1}
output: y_0, ..., y_{N-1}

for a = 0, N-1 do
    t = 0
    for b = 0, N-1 do
        t = t + x_a * exp(-2*PI*i*a*b/N)
    end
    y_a = t
end
```

Note that the point we're evaluating at is `exp(2*PI*i/N)`, the minus and the factor b make sure we pick the powers in the right order.

# 5. The fast Fourier transform

Suppose you want to multiply two polynomials of degree N, where N is big. The standard way to do this takes O(N^2) operations. Can we do better?

Evaluating a polynomial takes only O(N) operations if you do it right:

```
-- p = (p_0, p_1, ..., p_N) in coefficient form
function evaluate(p, N, x)
    t = 0
    f = 1
    for a = 0, N do
        t = t + f * p[a]
        f = f * x
    end
    return t
end
```

The point of the fast Fourier transform (FFT) is that multiplying two polynomials takes O(N) time too if the polynomials are given not as coefficients, but as evaluations at points: if A and B are polynomials, then `(A*B)(x) = A(x) * B(x)` e.g. if we represent a polynomial A as values `(a_0, a_1, ...)` that are evaluations at points, then

```
(a_0, a_1, a_2 ...) x (b_0, b_1, b_2, ...) = (a_0 * b_0, a_1 * b_1, ...)
```

So if we can Fourier-transform and invert the Fourier transform in less than N^2 time, we can multiply two polynomials as follows:

  1. Fourier-transform them both (which is essentially a basis transformation).
  2. In the new basis, just multiply the values.
  3. Invert the Fourier transform of the product.

And it turns out that for the particular points (roots of unity), we can do the transform and its inverse in O(N log N) time.

The trick is that if we think of the points as points `(b_0, ..., b_{N-1})`, then for the coefficients in the matrix 

```
[ 1            ...  1               ]
[ b_0          ...  b_{N-1}         ]
[ (b_0)^2      ...  (b_{N-1})^2     ]
[ ...          ...  ...             ]
[ (b_0)^{N-1}  ...  (b_{N-1})^{N-1} ]
```

we not only have dependencies down the columns, but across the rows too, 
because `b_u = (b_1)^u` where the `^u` is normal exponentiation.
Although we're evaluating at N different points, we can reuse most of the calculations and thus get the total from N^2 down to N log N.

There's also the technicality that multiplying two degree-N polynomials gives you a degree-2N result (unless you're in a finite field), but that doesn't bother us here.

# 6. Implementing FFT

The trick in the algorithm is to rewrite the polynomial as follows:

```
A(X) = a_0 + a_1 X + a_2 X^2 + a_3 X^3 ...
     = (a_0 + a_2 X^2 + a_4 X^4 ...) + X * (a_1 + a_3 X^2 + a_5 X^4 ...)
```

in terms of two polynomials with N/2 coefficients each, the odd ones and the even ones. If we now define

```
A_0(X) = a_0 + a_2 X + a_4 X^2 + ...
A_1(X) = a_1 + a_3 X + a_5 X^2 + ...
```

then we get the equation `A(X) = A_0(X^2) + X A_1(X^2)`.
This means we can evaluate the polynomial A of degree N at X by evaluating two polynomials of degree N/2 each at `X^2`, plus a bit of "adjusting" at the end.

Splitting a task of size N into two tasks of size N/2 is a classical algorithms trick to get `N log N` workload instead of `N^2` overall.
The reason that this works is the same one as for the mergesort algorithm.

The full pseudocode of the FFT algorithm (assuming N is even):

```
function fft(a, N)
    if N == 1 then return a end
    omega = 1
    omegaN = exp(2*PI*i/N)
    a0 = {}
    a1 = {}
    for u = 0, N-1 do
        if u % 2 == 0 then
            a0[u/2] = a[u]
        else
            a1[(u-1)/2] = a[u]
        end
    end
    -- recurse
    b0 = fft(a0, N/2)
    b1 = fft(a1, N/2)
    -- merge
    b = {}
    for u = 0, N/2 - 1
        b[u] = b0[u] + omega * b1[u]
        b[u+N/2] = b0[u] - omega * b1[u]
        omega = omega * omegaN
    end
    return b
end
```
